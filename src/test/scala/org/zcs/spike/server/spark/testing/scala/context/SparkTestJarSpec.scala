package org.zcs.spike.server.spark.testing.scala.context

import org.apache.spark.sql._
import org.apache.spark.sql.test.SharedSQLContext
import org.scalatest.Matchers
import org.zcs.spike.server.spark.testing.scala.repository.ApplesRepository

class SparkTestJarSpec extends QueryTest with SharedSQLContext with Matchers {

  val repository = new ApplesRepository

  import testImplicits._

  test("Sum When two apples Then sum of weights") {
    val weights = List(120, 150)
    val expected = weights.reduce((a, v) => a + v)
    val df = spark.createDataset(weights).toDF("weight")

    val actual = repository.mass(df)

    expected shouldBe actual
  }

}
