package org.zcs.spike.server.spark.testing.scala.context

import org.apache.spark.sql.test.SharedSparkSession
import org.scalatest.WordSpec
import org.zcs.spike.server.spark.testing.scala.repository.ApplesRepository

class WordSpecSessionSpec extends WordSpec with SharedSparkSession {

  import testImplicits._

  val repository = new ApplesRepository()

  "a Mass" when {
    "when only one apple" should {
      "have to return mass of this apple" in {
        val expected = 80
        val df = spark.createDataset(List(expected)).toDF("weight")
        assert(expected === repository.mass(df))
      }
    }

  }


}
