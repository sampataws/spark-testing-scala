package org.zcs.spike.server.spark.testing.scala.context

import org.apache.spark.sql.SparkSession
import org.scalatest.FunSuite

class ManualSpec extends FunSuite {

  test("Manual session creation") {
    val spark = SparkSession
      .builder
      .appName(getClass().getSimpleName)
      .master("local[2]")
      .getOrCreate()

    // use it

    spark.close()
  }


}
