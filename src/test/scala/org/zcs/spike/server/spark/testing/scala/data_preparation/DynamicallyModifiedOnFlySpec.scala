package org.zcs.spike.server.spark.testing.scala.data_preparation

import java.io.File

import org.apache.spark.sql.QueryTest
import org.apache.spark.sql.functions.lit
import org.apache.spark.sql.test.SharedSQLContext
import org.apache.spark.sql.types.DataTypes
import org.scalatest.Matchers
import org.zcs.spike.server.spark.testing.scala.repository.ApplesRepository

class DynamicallyModifiedOnFlySpec extends QueryTest with SharedSQLContext with Matchers {

  val repository = new ApplesRepository

  test("Mass When weight specified Then weights sum") {
    val expected = 85
    val input = getBulkData.withColumn("weight", lit(85))
    repository.mass(input) shouldBe expected
  }

  test("Mass When weight is null Then NPE") {
    val input = getBulkData.withColumn("weight", lit(null).cast(DataTypes.IntegerType))
    intercept[RuntimeException] {
      repository.mass(input)
    }

  }

  private def getBulkData = spark.read.option("header", "true").csv(getTestDataFolder)

  private def getTestDataFolder = {
    val dataRoot = "src/test/test-data"
    val testSubPath = this.getClass.getName.replaceAll("\\.", "/")
    new File(dataRoot, testSubPath).getPath
  }


}
